<?php

/**
 * Loader
 *
 * @package wp-fail2ban
 * @since 4.2.0
 */
namespace org\lecklider\charles\wordpress\wp_fail2ban;

if ( !defined( 'ABSPATH' ) ) {
    exit;
}
if ( defined( 'PHPUNIT_COMPOSER_INSTALL' ) ) {
    return;
}
/**
 * Helper
 *
 * @since 4.0.0
 *
 * @param string    $define
 * @param callable  $cast
 * @param bool      $unset
 * @param array     $field
 */
function _load(
    $define,
    $cast,
    $unset,
    array $field
)
{
    global  $wp_fail2ban ;
    $wp_fail2ban['config'][$define] = [
        'validate' => $cast,
        'unset'    => $unset,
        'field'    => $field,
        'ndef'     => !defined( $define ),
    ];
    if ( !defined( $define ) ) {
        
        if ( defined( "DEFAULT_{$define}" ) ) {
            // we've got a default
            define( $define, $cast( constant( "DEFAULT_{$define}" ) ) );
        } else {
            // bah
            define( $define, $cast( false ) );
        }
    
    }
}

/**
 * Validate IP list
 *
 * @since 4.0.0
 *
 * @param array|string  $value
 *
 * @return string
 */
function validate_ips( $value )
{
    return $value;
}

// phpcs:disable Generic.Functions.FunctionCallArgumentSpacing
_load(
    'WP_FAIL2BAN_AUTH_LOG',
    'intval',
    true,
    [ 'logging', 'authentication', 'facility' ]
);
_load(
    'WP_FAIL2BAN_LOG_COMMENTS',
    'boolval',
    true,
    [ 'logging', 'comments', 'enabled' ]
);
_load(
    'WP_FAIL2BAN_LOG_COMMENTS_EXTRA',
    'intval',
    true,
    [ 'logging', 'comments', 'extra' ]
);
_load(
    'WP_FAIL2BAN_COMMENT_LOG',
    'intval',
    false,
    [ 'logging', 'comments', 'facility' ]
);
_load(
    'WP_FAIL2BAN_COMMENT_EXTRA_LOG',
    'intval',
    false,
    [ 'logging', 'comments-extra', 'facility' ]
);
_load(
    'WP_FAIL2BAN_LOG_PASSWORD_REQUEST',
    'boolval',
    true,
    [ 'logging', 'password-request', 'enabled' ]
);
_load(
    'WP_FAIL2BAN_PASSWORD_REQUEST_LOG',
    'intval',
    false,
    [ 'logging', 'password-request', 'facility' ]
);
_load(
    'WP_FAIL2BAN_LOG_PINGBACKS',
    'boolval',
    true,
    [ 'logging', 'pingback', 'enabled' ]
);
_load(
    'WP_FAIL2BAN_PINGBACK_LOG',
    'intval',
    false,
    [ 'logging', 'pingback', 'facility' ]
);
_load(
    'WP_FAIL2BAN_LOG_SPAM',
    'boolval',
    true,
    [ 'logging', 'spam', 'enabled' ]
);
_load(
    'WP_FAIL2BAN_SPAM_LOG',
    'intval',
    false,
    [ 'logging', 'spam', 'facility' ]
);
_load(
    'WP_FAIL2BAN_OPENLOG_OPTIONS',
    'intval',
    true,
    [ 'syslog', 'connection' ]
);
_load(
    'WP_FAIL2BAN_SYSLOG_SHORT_TAG',
    'boolval',
    true,
    [ 'syslog', 'workaround', 'short_tag' ]
);
_load(
    'WP_FAIL2BAN_HTTP_HOST',
    'boolval',
    true,
    [ 'syslog', 'workaround', 'http_host' ]
);
_load(
    'WP_FAIL2BAN_TRUNCATE_HOST',
    'boolval',
    true,
    [ 'syslog', 'workaround', 'truncate_host' ]
);
_load(
    'WP_FAIL2BAN_BLOCK_USER_ENUMERATION',
    'boolval',
    true,
    [ 'block', 'user_enumeration' ]
);
_load(
    'WP_FAIL2BAN_BLOCKED_USERS',
    'strval',
    true,
    [ 'block', 'users' ]
);
_load(
    'WP_FAIL2BAN_PROXIES',
    __NAMESPACE__ . '\\validate_ips',
    true,
    [ 'remote-ip', 'proxies' ]
);
_load(
    'WP_FAIL2BAN_PLUGIN_LOG_AUTH',
    'boolval',
    true,
    [
    'logging',
    'plugins',
    'auth',
    'enabled'
]
);
_load(
    'WP_FAIL2BAN_PLUGIN_LOG_COMMENT',
    'boolval',
    true,
    [
    'logging',
    'plugins',
    'comment',
    'enabled'
]
);
_load(
    'WP_FAIL2BAN_PLUGIN_LOG_PASSWORD',
    'boolval',
    true,
    [
    'logging',
    'plugins',
    'password',
    'enabled'
]
);
_load(
    'WP_FAIL2BAN_PLUGIN_LOG_REST',
    'boolval',
    true,
    [
    'logging',
    'plugins',
    'rest',
    'enabled'
]
);
_load(
    'WP_FAIL2BAN_PLUGIN_LOG_SPAM',
    'boolval',
    true,
    [
    'logging',
    'plugins',
    'spam',
    'enabled'
]
);
_load(
    'WP_FAIL2BAN_PLUGIN_LOG_XMLRPC',
    'boolval',
    true,
    [
    'logging',
    'plugins',
    'xmlrpc',
    'enabled'
]
);
_load(
    'WP_FAIL2BAN_PLUGIN_AUTH_LOG',
    'intval',
    false,
    [
    'logging',
    'plugins',
    'auth',
    'facility'
]
);
_load(
    'WP_FAIL2BAN_PLUGIN_COMMENT_LOG',
    'intval',
    false,
    [
    'logging',
    'plugins',
    'comment',
    'facility'
]
);
_load(
    'WP_FAIL2BAN_PLUGIN_PASSWORD_LOG',
    'intval',
    false,
    [
    'logging',
    'plugins',
    'password',
    'facility'
]
);
_load(
    'WP_FAIL2BAN_PLUGIN_REST_LOG',
    'intval',
    false,
    [
    'logging',
    'plugins',
    'rest',
    'facility'
]
);
_load(
    'WP_FAIL2BAN_PLUGIN_SPAM_LOG',
    'intval',
    false,
    [
    'logging',
    'plugins',
    'spam',
    'facility'
]
);
_load(
    'WP_FAIL2BAN_PLUGIN_XMLRPC_LOG',
    'intval',
    false,
    [
    'logging',
    'plugins',
    'xmlrpc',
    'facility'
]
);
// phpcs:enable