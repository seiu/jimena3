<?php

/**
 * About
 *
 * @package wp-fail2ban
 * @since 4.2.0
 */
namespace org\lecklider\charles\wordpress\wp_fail2ban;

if ( !defined( 'ABSPATH' ) ) {
    exit;
}
/**
 * About content
 *
 * @since 4.2.0
 *
 * @param bool  $hide_title
 */
function about( $hide_title = false )
{
    $wp_f2b_ver = substr( WP_FAIL2BAN_VER, 0, strrpos( WP_FAIL2BAN_VER, '.' ) );
    ?>
<div class="wrap">
  <style>
    div.inside ul {
      list-style: disc;
      padding-left: 2em;
    }
  </style>
<?php 
    if ( !$hide_title ) {
        ?>
  <h1>WP fail2ban</h1>
<?php 
    }
    ?>
  <div id="poststuff">
    <div id="post-body" class="metabox-holder columns-2">
      <div id="post-body-content">
        <div class="meta-box-sortables ui-sortable">
          <div class="postbox">
            <h2>Version 4.2.3</h2>
            <div class="inside">
              <ul>
                <li><p>Workaround for some versions of PHP 7.x that would cause <tt>define()</tt>s to be ignored.</p></li>
                <li><p>Add config note to settings tabs.</p></li>
                <li><p>Fix documentation links.</p></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="meta-box-sortables ui-sortable">
          <div class="postbox">
            <h2>Version 4.2.2</h2>
            <div class="inside">
              <ul>
                <li><p>Fix 5.3 compatibility.</p></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="meta-box-sortables ui-sortable">
          <div class="postbox">
            <h2>Version 4.2.1</h2>
            <div class="inside">
              <ul>
                <li><p>Completed support for <tt><a href="https://docs.wp-fail2ban.com/en/4.2/defines/WP_FAIL2BAN_COMMENT_EXTRA_LOG.html" target="docs.wp-fail2ban.com">WP_FAIL2BAN_COMMENT_EXTRA_LOG</a></tt>.</p></li>
                <li><p>Add support for 3rd-party plugins; see <a href="https://docs.wp-fail2ban.com/en/4.2/developers.html" target="docs.wp-fail2ban.com">Developers</a>.</p>
                  <ul>
                    <li>Add-on for <a href="https://wordpress.org/plugins/wp-fail2ban-addon-contact-form-7/">Contact Form 7</a> (experimental).</li>
                    <li>Add-on for <a href="https://wordpress.org/plugins/wp-fail2ban-addon-gravity-forms/">Gravity Forms</a> (experimental).</li>
                  </ul>
                </li>
                <li><p>Change logging for known-user with incorrect password; previously logged as unknown user and matched by <tt>hard</tt> filters (due to limitations in older versions of WordPress), now logged as known user and matched by <tt>soft</tt>.</p></li>
                <li><p>Bugfix for email-as-username - now logged correctly and matched by <tt>soft</tt>, not <tt>hard</tt>, filters.</p></li>
                <li><p>Bugfix for regression in code to prevent Free/Premium conflict.</p></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div id="postbox-container-1" class="postbox-container">
        <div class="meta-box-sortables">
          <div class="postbox">
            <h2>Getting Started</h2>
            <div class="inside">
              <ol>
                <li><a href="https://docs.wp-fail2ban.com/en/<?php 
    echo  $wp_f2b_ver ;
    ?>/introduction.html" target="docs.wp-fail2ban.com">Introduction</a></li>
                <li><a href="https://docs.wp-fail2ban.com/en/<?php 
    echo  $wp_f2b_ver ;
    ?>/configuration.html" target="docs.wp-fail2ban.com">Configuration</a></li>
              </ol>
            </div>
          </div>
          <div class="postbox">
            <h2>Getting Help</h2>
            <div class="inside">
              <ul>
<?php 
    if ( wf_fs()->is_free_plan() ) {
        ?>
                <li><a href="https://wordpress.org/support/plugin/wp-fail2ban/" target="_blank">WordPress.org Forum</a></li>
<?php 
    }
    ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    &nbsp;
  </div>
</div>
<?php 
}
