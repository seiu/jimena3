<?php
define( 'wpacPLUGIN_URL', plugin_dir_url( __FILE__ ) );
$wpaac_bootStrapJS = wpacPLUGIN_URL."asset/js/bootstrap.min.js";
$wpaac_bootStrapCSS = wpacPLUGIN_URL."asset/css/bootstrap.min.css";
wp_register_script('wpaac-bootstrap_init', $wpaac_bootStrapJS);
wp_enqueue_script('wpaac-bootstrap_init');
wp_register_style('wpaac-bootstrapCSS_init', $wpaac_bootStrapCSS);
wp_enqueue_style('wpaac-bootstrapCSS_init');
if ( ! defined( 'wpaacbasedir' ) ) define( 'wpaacbasedir', plugin_dir_path( __FILE__ ) );
$cdndomain = site_url();
$cdndomain = preg_replace("(^https?://)", "", $cdndomain);
$cdndomain = str_replace("www.","",$cdndomain);
if(strpos($cdndomain,"/")>0)
{
	$cdnx = explode("/",$cdndomain);
	$cdndomain = $cdnx[0];
}
if(substr(phpversion(),0,3) < '5.4')
{
echo "<h2>ALERT: The plugin requires PHP version 5.4 or higher</h2>";
}
$cdnminttl = 0;
$cdnmaxttl = 0;
if($cdnminttl == "" || $cdnminttl == 0) $cdnminttl = 3600;
if($cdnmaxttl == "" || $cdnmaxttl == 0) $cdnmaxttl = 86400;
if(isset($_POST['senddebuglog']))
{
$from =  "awsplugin@" . $_SERVER['HTTP_HOST']; 
$to = "support@wpadmin.ca";
$sub = "Debug Log from " . $_SERVER['HTTP_HOST'];
$msg = $_REQUEST['wpadebuglog'];
$replyto =  get_bloginfo(admin_email);
$headers = 'From: DoNotReply@' . $_SERVER['HTTP_HOST'] . "\r\n" . 'Reply-To: ' . $replyto . "\r\n" . 'X-Mailer: PHP/' . phpversion();
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
if(wp_mail($to,$sub,$msg,$headers))
{
echo "<div class='notice notice-success is-dismissible'><h3>Debug log sent to the developer</h3></div>";
}
else
{
echo "<div class='notice notice-warning is-dismissible'><h3>Failed to send the debug log to developer</h3></div>";
}
}
?>
<div id=wpaac_WPAresult></div>
<div id=wpaac_WPAResult></div>
<div class=col-sm-6>
<h3> STEP I - Setup Cloudfront Distribution</h3>
<div class=col-sm-4>
<b>Access key ID:</b><br>
<input type=text id=wpaac_cdnak class=form-control placeholder="Access Key ID" value="">	
</div>
<div class=col-sm-4>
<b>Secret Key:</b><br>
<input type=text id=wpaac_cdnsk class=form-control placeholder="Secret Key" value="">	
</div>
<div class=col-sm-4>
<b>Domain Name:</b><br>
<?php
if ( is_multisite()  && is_super_admin()) 
{
?>
<select id=wpaac_cdnorigin class=form-control >
<?php	
$wpasites = get_sites();
foreach($wpasites as $key => $wpasite)
{
echo "<option value=\"".$wpasite->domain."\">".$wpasite->domain."</option>";
}
?>
</select>
<?php 
}
else
{
?>
<input type=text id=wpaac_cdnorigin class=form-control placeholder="Domain Name" value="<?php echo $cdndomain; ?>">	
<?php
}
?>
</div>
<div class=col-sm-4>
<b> Min Cache Time in seconds:</b><br>
<input type=number min=0 max=86400 step=60  id=wpaac_cdnminttl class=form-control placeholder="Minimum TTL" value="<?php echo $cdnminttl ; ?>">
</div>
<div class=col-sm-4>
<b> Max Cache Time  in seconds:</b><br>
<input type=number min=3600 max=2592000  step=60 id=wpaac_cdnmaxttl class=form-control placeholder="Maximum TTL" value="<?php echo $cdnmaxttl ; ?>">
</div>
<div class=col-sm-4>
<b>Price Class:</b><br>
<select id=wpaac_cdnprice class=form-control placeholder="Price Class">	
<option value='PriceClass_100'>US, Canada and Europe</option>
<option value='PriceClass_200'>US, Canada, Europe & Asia</option>
<option value='PriceClass_All'>All Locations</option>
</select>
<br>
</div>
<div class=col-sm-12>
<p>
<input type=checkbox id=wpaac_usecdn name=wpaac_usecdn> &nbsp; Instead of Amazon Cloudfront Domain name, I would like to use my custom domain: <b>cdn.<?php echo $cdndomain;?></b>
<div id=usecdn_notice style='display:none'>
<?php 
echo "<p><b>NOTE</b>: This feature needs an SSL certificate. The plugin will request a <b>Free</b> certificate from <a href='https://aws.amazon.com/certificate-manager/pricing/' target=_BLANK>Amazon Certificate Manager (ACM)</a>.</p><p>A verification email will be sent to any one of these email addresses: postmaster@$cdndomain, webmaster@$cdndomain, admin@$cdndomain, administrator@$cdndomain, hostmaster@$cdndomain.</p><p>Please ensure you have any one of these emails address / alias configured.</p>";
?>
</p>
</div>
</div>
<div class=col-sm-4>
<button id=wpaac_deployAWSCDN class='btn btn-info form-control'>Create Distribution</button>
</div>
<div class=col-sm-4>
<button id=wpaac_modifyAWSCDN class='btn btn-info form-control'>Modify Distribution</button>
</div>
<div class=col-sm-4>
<button id=wpaac_listAWSCDN class='btn btn-info form-control'>List Distribution</button>
</div>
<div class='col-sm-3 hidden'>
<button id=wpaac_renewcert class='btn btn-warning form-control'>Renew Certificate</button>
</div>
</div>
<?php 
$cdnurl =  get_option("WPAdmin_CDN_URL"); 
if($cdnurl == "Blank") $cdnurl = "";	
?>
<div class=col-sm-6>
<h3>STEP II - Activate Cloudfront on <?php echo $cdndomain; ?></h3>

<div class=col-sm-12>
<b>CDN DOMAIN / CNAME</b><br>
<input type=text id=wpaac_cdnurl class=form-control placeholder="CDN Domain" value="<?php echo $cdnurl; ?>">
<p>&nbsp;</p>
<?php
if(file_exists(wpaacbasedir . $cdndomain . ".txt"))
{
$cdn_details = file_get_contents(wpaacbasedir . $cdndomain . ".txt");
$cdn_details = substr($cdn_details,0,stripos($cdn_details,"cloudfront.net")+14);
$cdn_details = substr($cdn_details,strrpos($cdn_details,"<b>")+3);
echo '<div class=col-sm-6><p><a class="retval btn btn-success form-control" href="JavaScript:void(0);">Activate ' . $cdn_details . '</a></p></div><div class=col-sm-6><p><a class="retval btn btn-success form-control" href="JavaScript:void(0);">Activate cdn.' . $cdndomain . '</a></p></div><div class=col-sm-6><p><a class="retval btn btn-success form-control" href="JavaScript:void(0);">Disable CDN</a></p></div><div class=col-sm-6><p><button id=wpaac_resetcdn class="btn btn-danger form-control">Reset Configuration</button></p></div>
<p> [<a target=_BLANK href="http://' . $cdn_details . '">Test</a>]</p>';
}
else
{
/*echo "File Not Found:" . wpaacbasedir . $cdndomain . ".txt";	*/
}
?>
</div>
</div>
<div class=col-sm-8>
<h3>Do Not Load These Files From CDN [One entry per line]</h3>
<?php
$serverproto = "http";
if(@$_SERVER['HTTPS'] == "on") $serverproto = "https";
if(get_option('WPAdmin_CDN_Ignorelist')){
$wpaac_cdnignorelist = get_option('WPAdmin_CDN_Ignorelist');
$wpaac_cdnignorelist = str_replace("~wpa~",PHP_EOL,$wpaac_cdnignorelist);
}
?>
<textarea class='form-control textarea' style='min-height:150px;' id=wpaac_cdnignorelist name=wpaac_cdnignorelist placeholder='<?php echo $serverproto . "://" . $cdndomain; ?>/wp-content/plugins/aws-cdn-by-wpadmin/admin/asset/css/bootstrap.min.css'><?php echo $wpaac_cdnignorelist; ?></textarea>
</div>
<div class=col-sm-4>
<h3>My Site is Hosted in a Sub-Folder</h3>
<?php
$wpa_cdn_altdomain =  $_SERVER['HTTP_HOST'];
if(get_option('WPAdmin_CDN_AltDomain')){
$wpa_cdn_altdomain = get_option('WPAdmin_CDN_AltDomain');
}
?>
<input type=text class=form-control id=wpaac_cdn_altdomain name=wpaac_cdn_altdomain value='<?php echo $wpa_cdn_altdomain; ?>'>
<p>Only use this option if your WordPress site is hosted in a sub-folder</p>
<p>The sub-folder path should match the AWS <em>Origin</em>. Which means, if you open your AWS CDN Domain [<b>rAnd0mChA6s.cloudfront.net</b>] on the browser, it should redirect to <b>wpadmin.ca/subFolder.</b></p>
<p>Don't add the trailing slash. <b>E.G:</b> <?php echo $_SERVER['HTTP_HOST']; ?>/subFolder</p>
</div>
<div class=col-sm-12>
<h3>Howto / FAQ</h3>
<div class="tabbable" id="Bmmli">
<ul class="nav nav-tabs">
<li class="active">
<a href="#tab1" data-toggle="tab">
<p>How to</p>
</a>
</li>
<li>
<a href="#tab2" data-toggle="tab">
<p>FAQ</p>
</a>
</li>
</ul>
<div class="tab-content">
<div class="tab-pane active" id="tab1">
<p>&nbsp;</p>
<b>Setup CloudFront</b>
<ol>
<li>Setup your AWS Account @ <a href='http://aws.amazon.com/' target=_BLANK>aws.amazon.com</a></li>
<li>Refer to <a href='https://wpadmin.ca/how-to-create-an-aws-user-with-limited-permissions-to-access-cloudfront-only/' target=_BLANK>this article</a> to setup the correct permissions</li>
<li>Retrieve the <i>Access Key ID</i> & <i>Secret Key</i></li>
<li>Enter the <i>Access Key ID</i> & <i>Secret Key</i> in the respective input boxes on the left</li>
<li>The domain name is automatically listed, change if required</li>
<li>Select the <u>Price Class</u> (AWS charges may vary depending on your selection)</li>
<li>Click the <u>Create AWS Distribution</u> button</li>
<li>Wait for AWS to setup the Distribtuion. Check the progress by clicking the <u>List AWS Distribution</u> button</li>
<li>Enter the AWS assigned sub domain (<i>E.G: <small>rAnd0mChA6s.cloudfront.net</small></i>) in the <u>CDN DOMAIN / CNAME</u> box</li>
</ol>
<b>Disable Cloudfront Temporarily</b>
<ol>
<li>Click the <u>Disable CDN</u> button</li>
<li>Clear cache if you are using any caching plugin</li>
<li>Visit <a href='http://aws.amazon.com/' target=_BLANK>aws.amazon.com</a>, <i>Disable</i> the Distribution</li>
</ol>
<b>Re-enable Cloudfront</b>
<ol>
<li>Visit <a href='http://aws.amazon.com/' target=_BLANK>aws.amazon.com</a>, <i>Enable</i> the Distribution</li>
<li>Click the <b>Activate rAnd0mChA6s.cloudfront.net</b> OR <b>Activate cdn.<?php echo $cdndomain;?></b> button</li>
<li>Clear cache if you are using any caching plugin</li>
</ol>
<b>Delete Cloudfront Setup</b>
<ol>
<li>Visit <a href='http://aws.amazon.com/' target=_BLANK>aws.amazon.com</a>, <i>Disable the Cloudfront distribution</i></li><li>Click the <u>Reset Configuration</u> button</li>
</i> and then <i>Delete</i> the Distribution</li>
</ol>
</p>
</div>
<div class="tab-pane" id="tab2">
<p>
<dl>
<dt>How does the plugin work?</dt>
<dd>The plugin replaces the domain name on all static assets (images, scripts, stylesheets,etc) in the wp-content & wp-includes folder.</dd>
<dt>Does this plugin support WordPress Multisite Setup?</dt>
<dd>Yes, it does. If you have setup the multisite correctly and the <b>Domain Name:</b> field in STEP 1 shows a FQDN (Fully Qualified Domain Name), the plugin should work just fine.</dd>
<dt>Where are the AWS Access Key ID and Secret Key Stored?</dt>
<dd>The AWS Access Key ID and Secret Key is only used to communicate with AWS.Amazon.com and are not stored. It is your responsibility to keep them safe - do not share them with anyone.</dd>
<dt>I upgraded from a previous version, how do I fix CORS issue?</dt>
<dd>Version 1.3.7 has the code to fix CORS issue. Re-enter your <b>Access Key ID</b> & <b>Secret Key</b>, then click <u>Modify AWS Distribution</u>.</dd>
<dt>What does the <u>Reset Configuration</u> button do?</dt>
<dd>When you click the <u>Create AWS Distribution</u>. button, it checks if the file <b><?php echo $cdndomain;?>.txt</b> exists in the plugin folder.<br>
This stops the plugin from sending a duplicate request to Amazon (which will be declined anyway).<br>
The <u>Reset Configuration</u> button only deletes this file <br>
The CDN setup does not rely on this file.</dd>
<dt>What is stored in the <b><?php echo $cdndomain;?>.txt</b> file?</dt>
<dd>The message you see after a successful request to Amazon to create is Distribution is stored in the <b><?php echo $cdndomain;?>.txt</b> file</dd>
<dt>Can I use any other CDN?</dt>
<dd>Although not tested, if you have the domain name from any other CDN, Enter it in the <u>CDN DOMAIN / CNAME</u> box & it should work</dd>
<dt> What content is moved to AWS CDN</dt>
<dd> All Static files and images are moved to AWS CDN. There have been cases where some contents failed, please send me an email to report such issues</dd>
<dt> Can I  edit what goes and what does not?</dt>
<dd> Unfortunately, the plugin does not support granular control over contents that can be moved to AWS CDN</dd>
<dt>Is there a way to flush the CDN</dt>
<dd>Amazon refers this to '<b>Invalidation</b>' and charges for any invalidation requests. The easiest way is to rename the file or add a version tag</dd>
<dt>What if I have a few Questions?</dt>
<dd>Visit  <a href='http://wpadmin.ca?utm_source=Websites&utm_medium=WordPress&utm_campaign=WordPressCDNPlugin' target=_BLANK>WPAdmin.ca</a>, Chat with me If I am online or Leave a Message using the <a href='http://wpadmin.ca/contact-us/?utm_source=Websites&utm_medium=WordPress&utm_campaign=WordPressCDNPlugin' target=_BLANK>contact form</a>  </dd>
<dt>I don't get a response while trying to setup CDN</dt>
<dd>The plugin needs <em>php-xml</em> to process requests. This module is enabled by most hosting serivce providers. If you are using your own cloud server, please ensure the module is enabled on your server. </dd>
<dt>I want to buy you a coffee?</dt>
<dd>Thanks! <a href='https://wpadmin.ca/donation/' target=_BLANK>Please click Here to buy me one ;)</a></dd>
<dt>How do I stop the <b>Notice</b> from being displayed?</dt>
<dd>The plugin is 100% free & the ability to remove the <B>Notice</B> is only provided to our generous Donors. Please make a <a href='https://wpadmin.ca/donation/' target=_BLANK>Donation</a> to support this plugin, send us an email and we will provide you with your unique <B>Donation ID</b> to remove the <B>Notice</b> </a></dd>
</dl>
</p>
</div>
</div>
</div>
</div>
<script>
jQuery(document).ready(function(){

jQuery("#wpaac_usecdn").click(function(){
if(jQuery("#wpaac_usecdn").is(':checked'))
{
jQuery("#usecdn_notice").show();
}
else{
jQuery("#usecdn_notice").hide();
}
});

jQuery(".retval").live('click',function(){
var cdndomain = jQuery(this).text();
cdndomain = cdndomain.replace('Activate ','');
cdndomain = cdndomain.replace('Disable CDN','');
jQuery("#wpaac_cdnurl").val(cdndomain).focus();	
jQuery("#wpaac_cdnurl").trigger('blur');
});

jQuery("#wpaac_renewcert").click(function(){
var ak = jQuery("#wpaac_cdnak").val();
jQuery("#wpaac_cdnak").focus();
if(ak == "") return;
var sk = jQuery("#wpaac_cdnsk").val();
jQuery("#wpaac_cdnsk").focus();
if(sk == "") return;
var ori = jQuery("#wpaac_cdnorigin").val();
jQuery.ajax({
url: ajaxurl,
data: {
'action':'wpaac_renew_cert',
'wpaac_ak' : ak,
'wpaac_sk' : sk,
'wpaac_ori' : ori
},
success:function(data) {
jQuery("#wpaac_WPAresult").html(data);
},
error: function(errorThrown){
jQuery("#wpaac_WPAresult").html(errorThrown);
}
});

});

jQuery("#wpaac_validate").live('click',function(){
var ak = jQuery("#wpaac_cdnak").val();
jQuery("#wpaac_cdnak").focus();
if(ak == "") return;
var sk = jQuery("#wpaac_cdnsk").val();
jQuery("#wpaac_cdnsk").focus();
if(sk == "") return;
var ori = jQuery("#wpaac_cdnorigin").val();
jQuery.ajax({
url: ajaxurl,
data: {
'action':'wpaac_validation_email',
'wpaac_ak' : ak,
'wpaac_sk' : sk,
'wpaac_ori' : ori
},
success:function(data) {
jQuery("#wpaac_WPAresult").html(data);
},
error: function(errorThrown){
jQuery("#wpaac_WPAresult").html(errorThrown);
}
});

});

jQuery("#wpaac_listAWSCDN").click(function(){
var ak = jQuery("#wpaac_cdnak").val();
jQuery("#wpaac_cdnak").focus();
if(ak == "") return;
var sk = jQuery("#wpaac_cdnsk").val();
jQuery("#wpaac_cdnsk").focus();
if(sk == "") return;
var ori = jQuery("#wpaac_cdnorigin").val();
var ap = jQuery("#wpaac_cdnprice").val();
jQuery("#wpaac_listAWSCDN").focus();
jQuery.ajax({
url: ajaxurl,
data: {
'action':'wpaac_list_cdn',
'wpaac_ak' : ak,
'wpaac_sk' : sk,
'wpaac_ori' : ori
},
success:function(data) {
jQuery("#wpaac_WPAresult").html(data);
},
error: function(errorThrown){
jQuery("#wpaac_WPAresult").html(errorThrown);
}
});	
});
jQuery("#wpaac_resetcdn").click(function(){
var ori = jQuery("#wpaac_cdnorigin").val();
jQuery.ajax({
url: ajaxurl,
data: {
'action':'wpaac_reset_cdn',
'wpaac_ori' : ori
},
success:function(data) {
jQuery("#wpaac_WPAresult").html(data);
},
error: function(errorThrown){
jQuery("#wpaac_WPAresult").html(errorThrown);
}
});	
});
jQuery("#wpaac_modifyAWSCDN").click(function(){	
var ak = jQuery("#wpaac_cdnak").val();
jQuery("#wpaac_cdnak").focus();
if(ak == "") return;
var sk = jQuery("#wpaac_cdnsk").val();
jQuery("#wpaac_cdnsk").focus();
if(jQuery("#wpaac_usecdn").is(':checked'))
{
var wpaac_usecdn = "yes";
}
else
{
var wpaac_usecdn = "";
}
if(sk == "") return;
var ori = jQuery("#wpaac_cdnorigin").val();
var ap = jQuery("#wpaac_cdnprice").val();
var minttl = jQuery("#wpaac_cdnminttl").val();
var maxttl = jQuery("#wpaac_cdnmaxttl").val();
jQuery("#wpaac_modifyAWSCDN").focus();
jQuery.ajax({
url: ajaxurl,
data: {
'action':'wpaac_modify_cdn',
'wpaac_ak' : ak,
'wpaac_sk' : sk,
'wpaac_ap' : ap,
'wpaac_ori' : ori,
'wpaac_usecdn' : wpaac_usecdn,
'wpaac_minttl' : minttl,
'wpaac_maxttl' : maxttl
},
success:function(data) {
jQuery("#wpaac_WPAresult").html(data);
},
error: function(errorThrown){
jQuery("#wpaac_WPAresult").html(errorThrown);
}
});
});
jQuery("#wpaac_deployAWSCDN").click(function(){

if(jQuery("#wpaac_usecdn").is(':checked'))
{
var wpaac_usecdn = "yes";
}
else
{
var wpaac_usecdn = "";
}
	
var ak = jQuery("#wpaac_cdnak").val();
jQuery("#wpaac_cdnak").focus();
if(ak == "") return;
var sk = jQuery("#wpaac_cdnsk").val();
jQuery("#wpaac_cdnsk").focus();
if(sk == "") return;
var ori = jQuery("#wpaac_cdnorigin").val();
var ap = jQuery("#wpaac_cdnprice").val();
var minttl = jQuery("#wpaac_cdnminttl").val();
var maxttl = jQuery("#wpaac_cdnmaxttl").val();
jQuery("#wpaac_deployAWSCDN").focus();
jQuery("#wpaac_WPAresult").html("<div class='alert alert-info'>Please Wait While The Distribution is Being Created.</div>");
jQuery.ajax({
url: ajaxurl,
data: {
'action':'wpaac_deploy_cdn',
'wpaac_ak' : ak,
'wpaac_sk' : sk,
'wpaac_ap' : ap,
'wpaac_ori' : ori,
'wpaac_usecdn' : wpaac_usecdn,
'wpaac_minttl' : minttl,
'wpaac_maxttl' : maxttl
},
success:function(data) {
jQuery("#wpaac_WPAresult").html(data);
},
error: function(errorThrown){
jQuery("#wpaac_WPAresult").html(errorThrown);
}
});
});

jQuery("#wpaac_cdn_altdomain").blur(function(){
var wpaac_cdn_altdomain = jQuery(this).val();
jQuery.ajax({
url: ajaxurl,
data: {
'action':'wpaac_alt_domain',
'wpaac_cdn_altdomain' : wpaac_cdn_altdomain
},
success:function(data) {
jQuery("#wpaac_WPAresult").html(data);
},
error: function(errorThrown){
jQuery("#wpaac_WPAresult").html(errorThrown);
}
});
});


jQuery("#wpaac_cdnignorelist").blur(function(){
var wpaac_cdnignorelist = 	jQuery(this).val();
jQuery.ajax({
url: ajaxurl,
data: {
'action':'wpaac_ignore_list',
'wpaac_cdnignorelist' : wpaac_cdnignorelist
},
success:function(data) {
jQuery("#wpaac_WPAresult").html(data);
},
error: function(errorThrown){
jQuery("#wpaac_WPAresult").html(errorThrown);
}
});
});

jQuery("#wpaac_cdnurl").blur(function(){
var cdnurl = jQuery(this).val();
jQuery.ajax({
url: ajaxurl,
data: {
'action':'wpaac_set_cdn',
'wpaac_cdnurl' : cdnurl
},
success:function(data) {
jQuery("#wpaac_WPAresult").html(data);
},
error: function(errorThrown){
jQuery("#wpaac_WPAresult").html(errorThrown);
}
});
});
});
</script>
